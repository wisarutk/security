package security

import "testing"

func TestEncryptAndDecrypt(t *testing.T) {
	text := "test"
	ciphertext, err := Encrypt([]byte(text), "Hello")
	if err != nil {
		t.Fatal(err)
	}

	_, err = Decrypt(ciphertext, "Hello")
	if err != nil {
		t.Fatal(err)
	}
}

// func TestDecrypt(t *testing.T) {
// 	text := "test"
// 	_, err := Decrypt([]byte(text), "Hello")
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// }
